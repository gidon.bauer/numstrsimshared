# Numerische Strömungssimulation

Gruppe: Stefan Basermann, Gidon Bauer

# TODO

- [x] Algebraischen Ansatz zum laufen bekommen, bzw. den Fehler finden in einem der beiden Ansätze
  - [x] Untersuchen warum die Ergebniss abweichen
  - [ ] Randbedingungen im Algebraischen Ansatz
    - [x] Dirichlet
    - [ ] Neumann
- [x] Gittergenerator
  - [x] neumannFlux fixen -> funktioniert mit dem GNU-Compiler, nicht mit AppleClang
  - [x] Anfangswerte einlesen
  - [x] Sinnvolle Werte für bType_p finden
- [ ] Geschwindigkeitsfeld berechnen
- [ ] Druckfeld berechnen

# How to create build and compile code:

1. mkdir build
2. cd build
3. cmake ..
4. make install


