%% Clear the workspace
clear; clc; close all;

%% Read in the output of our simualtion
% Grid with the values of x
gridX = readmatrix("OutMatlab/gridX.dat");
% Grid with the values of y
gridY = readmatrix("OutMatlab/gridY.dat");
% Values of the potenial fuction
s = readmatrix("OutMatlab/scalar.dat");
% x component of the velocity
u = readmatrix("OutMatlab/u.dat");
% y component of the velocity
v = readmatrix("OutMatlab/v.dat");
% pressure coefficient
cp = readmatrix("OutMatlab/cp.dat");

%% Setup figure for plots
fig = figure(1);
fig.WindowState = 'maximized';
tiledlayout(2,2);
%% Plot the potential function
nexttile;
s_plot = pcolor(gridX,gridY,s);
s_plot.FaceColor = 'interp';
% surf(gridX,gridY,s);
% contour(gridX,gridY,s);
title('\phi');
xlabel('x');
ylabel('y');
zlabel('\phi');
colorbar('eastoutside');

%% Plot the pressure coefficient
nexttile;
cp_plot = pcolor(gridX,gridY,cp);
cp_plot.FaceColor = 'interp';
% surf(gridX,gridY,cp);
title('c_p');
xlabel('x');
ylabel('y');
zlabel('c_p');
colorbar('eastoutside');

%% Plot the x component of the velocity
nexttile;
u_plot = pcolor(gridX,gridY,u);
u_plot.FaceColor = 'interp';
% surf(gridX,gridY,u);
title('u');
xlabel('x');
ylabel('y');
zlabel('u');
colorbar('eastoutside');

%% Plot the y component of the velocity
nexttile;
v_plot = pcolor(gridX,gridY,v);
v_plot.FaceColor = 'interp';
% surf(gridX,gridY,v);
title('v');
xlabel('x');
ylabel('y');
zlabel('v');
colorbar('eastoutside');

%% Plot velocity
figure(2);
quiver(gridX,gridY,u,v);
title('velocity');
xlabel('x');
ylabel('y');
