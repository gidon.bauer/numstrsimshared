%% Clean up
clc; clear; close all;

%% Read in the grid
gridX = readmatrix('OutMatlab/gridX.dat');
gridY = readmatrix('OutMatlab/gridY.dat');
[numX, numY] = size(gridX);

%% Plot the x-y grid
figure(1);
hold on;
for i = 1:numX
    plot(gridX(i,:), gridY(i,:), 'Color', '#d3d3d3');
end

for i = 1:numY
    plot(gridX(:,i), gridY(:,i), 'Color', '#d3d3d3');
end

for i = 1:numX
    for j = 1:numY
        plot(gridX(i,j), gridY(i,j), '.', 'Color', 'r', 'MarkerSize', 12);
    end
end

% Schrittweite bei der Achsenskalierung
set(gca,'XTick',[0:0.25:1]);
set(gca,'YTick',[0:0.25:1]);

xlabel('x');
ylabel('y');
title('x-y Gitter');
hold off;

%% Calculate the cartesian xi-eta grid
dxi = 1;
deta = 1;
xiMax = (numX-1) * dxi;
etaMax = (numY-1) * deta;

gridXi = zeros(numX, numY);
gridEta = zeros(numX, numY);

for i = 1:numX
    for j = 1:numY
        gridXi(i,j) = (i-1) * dxi;
        gridEta(i,j) = (j-1) * deta;
    end
end

%% Plot the cartesian xi-eta grid
figure(2);
hold on;
for i = 1:numX
    plot(gridXi(i,:), gridEta(i,:), 'Color', '#d3d3d3');
end

for i = 1:numY
    plot(gridXi(:,i), gridEta(:,i), 'Color', '#d3d3d3');
end

for i = 1:numX
    for j = 1:numY
        plot(gridXi(i,j), gridEta(i,j), '.', 'Color', 'r', 'MarkerSize', 12);
    end
end

% Schrittweite bei der Achsenskalierung
set(gca,'XTick',[0:1:xiMax]);
set(gca,'YTick',[0:1:etaMax]);

xlabel('\xi');
ylabel('\eta');
title('\xi-\eta Gitter');
hold off;
