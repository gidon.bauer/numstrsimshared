/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   jens.henrik.goebbert@itv.rwth-aachen.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "solve.h"

#include <math.h>

#include <iostream>

#include "data.h"
#include "metrics.h"
#include "setup.h"

//-----------------------------------------------------
bool solve(sData* data) {
  std::cout << "\nSolve:\n-------\n";

  if (!gaussseidel(data, data->s1)) {
    return false;
  }
  // if (!jacobi(data, data->s1)) {
  //   return false;
  // }

  return true;
}

//--- Gauss-Seidel solver -----------------------------
bool gaussseidel(sData* data, double** s) {
  int curIter = 0;
  double curResidual = MAXDOUBLE;
  double curMaxResidual = MAXDOUBLE;

  std::cerr << "\r\tRunning Gauss-Seidel solver... ";
  while (curIter < data->maxIter && ABS(curMaxResidual) > data->maxResidual) {
    curIter++;

    // -Randbedingungen unten und oben berechnen-------------------------------

    for (int i = 1; i < data->nX - 1; ++i) {
      // -Oben---------------------------------------------------------------
      double dxidx_up =
          (Xi(data, data->x[i + 1][data->nY - 1], data->y[i][data->nY - 1]) -
           Xi(data, data->x[i - 1][data->nY - 1], data->y[i][data->nY - 1])) /
          (2 * (data->x[i + 1][data->nY - 1] - data->x[i][data->nY - 1]));
      // -------------------
      double dxidy_up =
          (3 * Xi(data, data->x[i][data->nY - 1], data->y[i][data->nY - 1]) -
           4 * Xi(data, data->x[i][data->nY - 1], data->y[i][data->nY - 2]) +
           Xi(data, data->x[i][data->nY - 1], data->y[i][data->nY - 3])) /
          (2 * (data->y[i][data->nY - 1] - data->y[i][data->nY - 2]));
      // -------------------
      double detadx_up =
          (Et(data, data->x[i + 1][data->nY - 1], data->y[i][data->nY - 1]) -
           Et(data, data->x[i - 1][data->nY - 1], data->y[i][data->nY - 1])) /
          (2 * (data->x[i + 1][data->nY - 1] - data->x[i][data->nY - 1]));
      // -------------------
      double detady_up =
          (3 * Et(data, data->x[i][data->nY - 1], data->y[i][data->nY - 1]) -
           4 * Et(data, data->x[i][data->nY - 1], data->y[i][data->nY - 2]) +
           Et(data, data->x[i][data->nY - 1], data->y[i][data->nY - 3])) /
          (2 * (data->y[i][data->nY - 1] - data->y[i][data->nY - 2]));
      // -------------------
      double dhdx_up =
          (ContourMax(data->x[i + 1][data->nY - 1]) -
           ContourMax(data->x[i - 1][data->nY - 1])) /
          (2 * (data->x[i + 1][data->nY - 1] - data->x[i][data->nY - 1]));
      // -------------------
      double dphidxi_up =
          (1.0 / 2.0) * (s[i + 1][data->nY - 1] - s[i - 1][data->nY - 1]);
      // -------------------
      s[i][data->nY - 1] =
          (2.0 / 3.0) *
              ((dxidy_up - dhdx_up * dxidx_up) /
               (dhdx_up * detadx_up - detady_up)) *
              dphidxi_up +
          (1.0 / 3.0) * (-s[i][data->nY - 3] + 4 * s[i][data->nY - 2]);

      // -Unten--------------------------------------------------------------
      // -------------------
      double dxidx_down = (Xi(data, data->x[i + 1][0], data->y[i][0]) -
                           Xi(data, data->x[i - 1][0], data->y[i][0])) /
                          (2 * (data->x[i + 1][0] - data->x[i][0]));
      // -------------------
      double dxidy_down = (-3 * Xi(data, data->x[i][0], data->y[i][0]) +
                           4 * Xi(data, data->x[i][0], data->y[i][1]) -
                           Xi(data, data->x[i][data->nY - 1], data->y[i][2])) /
                          (2 * (data->y[i][1] - data->y[i][0]));
      // -------------------
      double detadx_down = (Et(data, data->x[i + 1][0], data->y[i][0]) -
                            Et(data, data->x[i - 1][0], data->y[i][0])) /
                           (2 * (data->x[i + 1][0] - data->x[i][0]));
      // -------------------
      double detady_down = (-3 * Et(data, data->x[i][0], data->y[i][0]) +
                            4 * Et(data, data->x[i][0], data->y[i][1]) -
                            Et(data, data->x[i][0], data->y[i][2])) /
                           (2 * (data->y[i][1] - data->y[i][0]));
      // -------------------
      double dhdx_down =
          (ContourMin(data->x[i + 1][0]) - ContourMin(data->x[i - 1][0])) /
          (2 * (data->x[i + 1][0] - data->x[i][0]));
      // -------------------
      double dphidxi_down = (1.0 / 2.0) * (s[i + 1][0] - s[i - 1][0]);
      // -------------------
      s[i][0] = -(2.0 / 3.0) *
                    ((dxidy_down - dhdx_down * dxidx_down) /
                     (dhdx_down * detadx_down - detady_down)) *
                    dphidxi_down -
                (1.0 / 3.0) * (s[i][2] - 4 * s[i][1]);
    }

    // ------------------------------------------------------------------------

    curMaxResidual = 0;
    for (int i = 1; i < data->nX - 1; i++) {
      for (int j = 1; j < data->nY - 1; j++) {
        /* Old residual
        curResidual =
            -((1 / (dx * dx)) * (s[i + 1][j] - 2 * s[i][j] + s[i - 1][j]) +
              (1 / (dy * dy)) * (s[i][j + 1] - 2 * s[i][j] + s[i][j - 1]));
        */

        // d(xi) = d(eta) = 1
        curResidual =
            -(data->alpha1[i][j] * (s[i + 1][j] - 2 * s[i][j] + s[i - 1][j]) +
              data->alpha2[i][j] * (s[i][j + 1] - 2 * s[i][j] + s[i][j - 1]) +
              (data->alpha3[i][j] / 4) * (s[i + 1][j + 1] - s[i - 1][j + 1] -
                                          s[i + 1][j - 1] + s[i - 1][j - 1]) +
              (data->alpha4[i][j] / 2) * (s[i + 1][j] - s[i - 1][j]) +
              (data->alpha5[i][j] / 2) * (s[i][j + 1] - s[i][j - 1]));

        s[i][j] -=
            curResidual / (2 * (data->alpha1[i][j] + data->alpha2[i][j]));

        if (ABS(curResidual) > ABS(curMaxResidual))
          curMaxResidual = ABS(curResidual);
      }
    }
  }
  std::cout << "done." << std::endl;
  std::cout << "\n\tGauss-Seidel iterations: " << curIter << std::endl;
  std::cout << "\n\tMaximum residuum       : " << curMaxResidual << std::endl;

  return true;
}

//--- Jacobi solver -----------------------------------
bool jacobi(sData* data, double** s) {
  int curIter = 0;
  double curResidual = MAXDOUBLE;
  double curMaxResidual = MAXDOUBLE;

  std::cout << "\r\tRunning Jacobi solver... ";
  // Array mit den neuen Werten für s, damit curResidual mit den alten Werten
  // berechnet wird
  double s_new[data->nX][data->nY];
  for (int i = 1; i < data->nX - 1; ++i) {
    for (int j = 1; j < data->nY - 1; ++j) {
      s_new[i][j] = s[i][j];
    }
  }
  while (curIter < data->maxIter && ABS(curMaxResidual) > data->maxResidual) {
    curIter++;

    curMaxResidual = 0;
    for (int i = 1; i < data->nX - 1; i++) {
      for (int j = 1; j < data->nY - 1; j++) {
        /* Old residual
        curResidual =
            -((1 / (dx * dx)) * (s[i + 1][j] - 2 * s[i][j] + s[i - 1][j]) +
              (1 / (dy * dy)) * (s[i][j + 1] - 2 * s[i][j] + s[i][j - 1]));
        */

        // d(xi) = d(eta) = 1
        curResidual =
            -(data->alpha1[i][j] * (s[i + 1][j] - 2 * s[i][j] + s[i - 1][j]) +
              data->alpha2[i][j] * (s[i][j + 1] - 2 * s[i][j] + s[i][j - 1]) +
              data->alpha3[i][j] * ((s[i + 1][j + 1] - s[i - 1][j + 1] -
                                     s[i + 1][j - 1] + s[i - 1][j - 1]) /
                                    4) +
              data->alpha4[i][j] * ((s[i + 1][j] - s[i - 1][j]) / 2) +
              data->alpha5[i][j] * ((s[i][j + 1] - s[i][j - 1]) / 2));

        s_new[i][j] -=
            curResidual / (2 * (data->alpha1[i][j] + data->alpha2[i][j]));

        if (ABS(curResidual) > ABS(curMaxResidual))
          curMaxResidual = ABS(curResidual);
      }
    }
    for (int i = 1; i < data->nX - 1; ++i) {
      for (int j = 1; j < data->nY - 1; ++j) {
        s[i][j] = s_new[i][j];
      }
    }
  }
  std::cout << "done." << std::endl;
  std::cout << "\n\tJacobi iterations: " << curIter << std::endl;
  std::cout << "\n\tMaximum residuum : " << curMaxResidual << std::endl;

  return true;
}

//--- Thomas algorithm --------------------------------
bool thomas(sData* data, double** s) {
  // optional
  return true;
}

//--- Velocity ----------------------------------------
bool calc_vel(sData* data) {
  std::cerr << "\n\r\tCalculate the velocity... ";
  // Oben:
  for (int i = 1; i < data->nX - 1; ++i) {
    // Using interpolation
    data->u[i][data->nY - 1] =
        (s_inter(data, data->x[i + 1][data->nY - 1], data->y[i][data->nY - 1]) -
         s_inter(data, data->x[i - 1][data->nY - 1],
                 data->y[i][data->nY - 1])) /
        (2 * (data->x[i + 1][data->nY - 1] - data->x[i][data->nY - 1]));

    // data->u[i][data->nY - 1] =
    //     (data->s1[i + 1][data->nY - 1] - data->s1[i - 1][data->nY - 1]) /
    //     (2 * (data->x[i + 1][data->nY - 1] - data->x[i][data->nY - 1]));

    data->v[i][data->nY - 1] =
        (3 * data->s1[i][data->nY - 1] - 4 * data->s1[i][data->nY - 2] +
         data->s1[i][data->nY - 3]) /
        (2 * (data->y[i][data->nY - 1] - data->y[i][data->nY - 2]));
  }

  // Unten:
  for (int i = 1; i < data->nX - 1; ++i) {
    // Using interpolation
    data->u[i][0] = (s_inter(data, data->x[i + 1][0], data->y[i][0]) -
                     s_inter(data, data->x[i - 1][0], data->y[i][0])) /
                    (2 * (data->x[i + 1][0] - data->x[i][0]));

    // data->u[i][0] = (data->s1[i + 1][0] - data->s1[i - 1][0]) /
    //                 (2 * (data->x[i + 1][0] - data->x[i][0]));

    data->v[i][0] =
        (-3 * data->s1[i][0] + 4 * data->s1[i][1] - data->s1[i][2]) /
        (2 * (data->y[i][1] - data->y[i][0]));
  }

  // Links:
  for (int i = 1; i < data->nY - 1; ++i) {
    // Using interpolation
    data->u[0][i] = (-3 * s_inter(data, data->x[0][i], data->y[0][i]) +
                     4 * s_inter(data, data->x[1][i], data->y[0][i]) -
                     s_inter(data, data->x[2][i], data->y[0][i])) /
                    (2 * (data->x[1][i] - data->x[0][i]));

    data->v[0][i] = (data->s1[0][i + 1] - data->s1[0][i - 1]) /
                    (2 * (data->y[0][i + 1] - data->y[0][i]));
  }

  // Rechts:
  for (int i = 1; i < data->nY - 1; ++i) {
    // Using interpolation
    data->u[data->nX - 1][i] =
        (3 * s_inter(data, data->x[data->nX - 1][i], data->y[data->nX - 1][i]) -
         4 * s_inter(data, data->x[data->nX - 2][i], data->y[data->nX - 1][i]) +
         s_inter(data, data->x[data->nX - 3][i], data->y[data->nX - 1][i])) /
        (2 * (data->x[data->nX - 1][i] - data->x[data->nX - 2][i]));

    data->v[data->nX - 1][i] =
        (data->s1[0][i + 1] - data->s1[0][i - 1]) /
        (2 * (data->y[data->nX - 1][i] - data->y[data->nX - 1][i - 1]));
  }

  // Ecken:
  // Oben links:
  // Using interpolation
  data->u[0][data->nY - 1] =
      (-3 * s_inter(data, data->x[0][data->nY - 1], data->y[0][data->nY - 1]) +
       4 * s_inter(data, data->x[1][data->nY - 1], data->y[0][data->nY - 1]) -
       s_inter(data, data->x[2][data->nY - 1], data->y[0][data->nY - 1])) /
      (2 * (data->x[1][data->nY - 1] - data->x[0][data->nY - 1]));

  data->v[0][data->nY - 1] =
      (3 * data->s1[0][data->nY - 1] - 4 * data->s1[0][data->nY - 2] +
       data->s1[0][data->nY - 3]) /
      (2 * (data->y[0][data->nY - 1] - data->y[0][data->nY - 2]));

  // Unten links:
  // Using interpolation
  data->u[0][0] = (-3 * s_inter(data, data->x[0][0], data->y[0][0]) +
                   4 * s_inter(data, data->x[1][0], data->y[0][0]) -
                   s_inter(data, data->x[2][0], data->y[0][0])) /
                  (2 * (data->x[1][0] - data->x[0][0]));
  // data->u[0][0] = (-3 * data->s1[0][0] + 4 * data->s1[1][0] - data->s1[2][0])
  // /
  //                 (2 * (data->x[1][0] - data->x[0][0]));

  data->v[0][0] = (-3 * data->s1[0][0] + 4 * data->s1[0][1] - data->s1[0][2]) /
                  (2 * (data->y[0][1] - data->y[0][0]));

  // Oben rechts:
  // Using interpolation
  data->u[data->nX - 1][data->nY - 1] =
      (3 * s_inter(data, data->x[data->nX - 1][data->nY - 1],
                   data->y[data->nX - 1][data->nY - 1]) -
       4 * s_inter(data, data->x[data->nX - 2][data->nY - 1],
                   data->y[data->nX - 1][data->nY - 1]) +
       s_inter(data, data->x[data->nX - 3][data->nY - 1],
               data->y[data->nX - 1][data->nY - 1])) /
      (2 * (data->x[data->nX - 1][data->nY - 1] -
            data->x[data->nX - 2][data->nY - 1]));

  data->v[data->nX - 1][data->nY - 1] =
      (3 * data->s1[data->nX - 1][data->nY - 1] -
       4 * data->s1[data->nX - 1][data->nY - 2] +
       data->s1[data->nX - 1][data->nY - 3]) /
      (2 * (data->y[data->nX - 1][data->nY - 1] -
            data->y[data->nX - 1][data->nY - 2]));

  // Unten rechts:
  // Using interpolation
  data->u[data->nX - 1][0] =
      (3 * s_inter(data, data->x[data->nX - 1][0], data->y[data->nX - 1][0]) -
       4 * s_inter(data, data->x[data->nX - 2][0], data->y[data->nX - 1][0]) +
       s_inter(data, data->x[data->nX - 3][0], data->y[data->nX - 1][0])) /
      (2 * (data->x[data->nX - 1][0] - data->x[data->nX - 2][0]));

  data->v[data->nX - 1][0] =
      (-3 * data->s1[data->nX - 1][0] + 4 * data->s1[data->nX - 1][1] -
       data->s1[data->nX - 1][2]) /
      (2 * (data->y[data->nX - 1][1] - data->y[data->nX - 1][0]));

  for (int i = 1; i < data->nX - 1; ++i) {
    for (int j = 1; j < data->nY - 1; ++j) {
      // Using interpolation
      data->u[i][j] = (s_inter(data, data->x[i + 1][j], data->y[i][j]) -
                       s_inter(data, data->x[i - 1][j], data->y[i][j])) /
                      (2 * (data->x[i + 1][j] - data->x[i][j]));

      // data->u[i][j] = (data->s1[i + 1][j] - data->s1[i - 1][j]) /
      //                 (2 * (data->x[i + 1][j] - data->x[i][j]));
      data->v[i][j] = (data->s1[i][j + 1] - data->s1[i][j - 1]) /
                      (2 * (data->y[i][j + 1] - data->y[i][j]));
    }
  }

  // Set uInfty to match velocity of incoming fluid
  data->uInfty = data->u[0][0];

  std::cout << "done." << std::endl;
  return true;
}

//---Interpoliert s in y-Richtung----------------------------------------------
double s_inter(sData* data, double x, double y) {
  // Find grid position for x
  int x_pos = 0;
  while (abs(x - data->x[x_pos][0]) >= 1e-6) {
    if (x_pos >= data->nX) throw std::runtime_error("Invalid x position.");
    ++x_pos;
  }
  // Find grid positions for y
  int y_lower = 0, y_upper = 0;
  while ((y - data->y[x_pos][y_upper]) > 1e-6) {
    if (y_upper >= data->nY) {
      y_upper = data->nY - 1;
      break;
      // throw std::runtime_error("Invalid y position.");
    }
    ++y_upper;
  }
  y_lower = y_upper - 1;

  // interpolate
  double sol = data->s1[x_pos][y_lower] +
               ((data->s1[x_pos][y_upper] - data->s1[x_pos][y_lower]) /
                (data->y[x_pos][y_upper] - data->y[x_pos][y_lower])) *
                   (y - data->y[x_pos][y_lower]);

  return sol;
}

//--- Pressure ----------------------------------------------------------------
bool calc_cp(sData* data) {
  std::cout << "\n\r\tCalculate the pressure coefficient... ";
  for (int i = 0; i < data->nX; ++i) {
    for (int j = 0; j < data->nY; ++j) {
      data->cp[i][j] = 1 - (pow(data->u[i][j], 2) + pow(data->v[i][j], 2)) /
                               (pow(data->uInfty, 2) + pow(data->vInfty, 2));
    }
  }

  std::cout << "done." << std::endl;

  // std::cout << "cp:\n";
  // for (int i = 0; i < data->nX; ++i) {
  //   for (int j = 0; j < data->nY; ++j) {
  //     std::cout << data->cp[i][j] << " ";
  //   }
  //   std::cout << "\n";
  // }

  return true;
}
