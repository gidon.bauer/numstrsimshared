/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   jens.henrik.goebbert@itv.rwth-aachen.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "setup.h"

#include <math.h>

#include <iostream>

#include "data.h"
#include "metrics.h"

//-----------------------------------------------------
bool setup(sData* data) {
  std::cout << "\nSetup:\n-------\n";

  // setup x/y grid
  // x-y Grid entsprechend der Contour aufstellen
  std::cout << "\tCreate grid... ";
  double dx = (data->xMax - data->xMin) / (data->nX - 1);
  for (int i = 0; i < data->nX; i++) {
    for (int j = 0; j < data->nY; j++) {
      data->x[i][j] = data->xMin + i * dx;
      // Calculate a new dy
      double dy = (ContourMax(data->x[i][j]) - ContourMin(data->x[i][j])) /
                  (data->nY - 1);
      data->y[i][j] = ContourMin(data->x[i][0]) + j * dy;
    }
  }
  std::cout << "done.\n\n";

  // set inital values of scalar s1
  std::cout << "\tSet initial values... ";
  for (int i = 0; i < data->nX; i++) {
    for (int j = 0; j < data->nY; j++) {
      data->s1[i][j] = 42;
    }
  }
  std::cout << "done.\n\n";

  //--- sub task 2 ---------------------------------------
  // setup xi/eta grid
  std::cout << "\tCreate Xi-Eta grid... ";
  for (int i = 0; i < data->nX; i++) {
    for (int j = 0; j < data->nY; j++) {
      // FIXME (Subtask 2)
      // using d(xi) = d(eta) = 1
      data->xi[i][j] = i;
      data->eta[i][j] = j;
    }
  }
  std::cout << "done.\n\n";

  // precompute derivatives for coordinate transformation
  std::cout << "\tPrecompute derivatives for coordinate transformation... ";
  for (int i = 1; i < data->nX - 1; i++) {
    for (int j = 1; j < data->nY - 1; j++) {
      // First order:
      // -------------------
      double dxidx = (Xi(data, data->x[i + 1][j], data->y[i][j]) -
                      Xi(data, data->x[i - 1][j], data->y[i][j])) /
                     (2 * dx);
      // -------------------
      double dxidy = (Xi(data, data->x[i][j], data->y[i][j + 1]) -
                      Xi(data, data->x[i][j], data->y[i][j - 1])) /
                     (2 * (data->y[i][j + 1] - data->y[i][j]));
      // -------------------
      double detadx = (Et(data, data->x[i + 1][j], data->y[i][j]) -
                       Et(data, data->x[i - 1][j], data->y[i][j])) /
                      (2 * dx);
      // -------------------
      double detady = (Et(data, data->x[i][j], data->y[i][j + 1]) -
                       Et(data, data->x[i][j], data->y[i][j - 1])) /
                      (2 * (data->y[i][j + 1] - data->y[i][j]));

      // Second order:
      // -------------------
      double dxidxx = (Xi(data, data->x[i + 1][j], data->y[i][j]) -
                       2 * Xi(data, data->x[i][j], data->y[i][j]) +
                       Xi(data, data->x[i - 1][j], data->y[i][j])) /
                      pow(dx, 2);
      // -------------------
      double dxidyy = (Xi(data, data->x[i][j], data->y[i][j + 1]) -
                       2 * Xi(data, data->x[i][j], data->y[i][j]) +
                       Xi(data, data->x[i][j], data->y[i][j - 1])) /
                      pow(data->y[i][j + 1] - data->y[i][j], 2);
      // -------------------
      double detadxx = (Et(data, data->x[i + 1][j], data->y[i][j]) -
                        2 * Et(data, data->x[i][j], data->y[i][j]) +
                        Et(data, data->x[i - 1][j], data->y[i][j])) /
                       pow(dx, 2);
      // -------------------
      double detadyy = (Et(data, data->x[i][j], data->y[i][j + 1]) -
                        2 * Et(data, data->x[i][j], data->y[i][j]) +
                        Et(data, data->x[i][j], data->y[i][j - 1])) /
                       pow(data->y[i][j + 1] - data->y[i][j], 2);
      // -------------------
      data->alpha1[i][j] = pow(dxidx, 2) + pow(dxidy, 2);
      data->alpha2[i][j] = pow(detadx, 2) + pow(detady, 2);
      data->alpha3[i][j] = 2 * (dxidx * detadx + dxidy * detady);
      data->alpha4[i][j] = dxidxx + dxidyy;
      data->alpha5[i][j] = detadxx + detadyy;
    }
  }
  std::cout << "done.\n\n";

  //-boundary conditions-------------------------------------------------------

  // TODO: Randbedingungen fuer gegebens Beispiel

  // set boundary conditions for scalar s1
  std::cout << "\tSet boundary conditions... ";
  if (data->potentialFunc == EXAMPLE) {
    // Berechnung der Ausgangsgeschwindigkeit
    // double Ain = ContourMax(data->x[0][0]) - ContourMin(data->x[0][0]);
    // double Aout = ContourMax(data->x[data->nX - 1][data->nY - 1]) -
    //               ContourMin(data->x[data->nX - 1][data->nY - 1]);
    // double uOut = (Ain / Aout) * data->uInfty;
    // Links + Rechts
    for (int i = 0; i < data->nY; ++i) {
      // Links -> Parallele Anströmung mit uInfty und vInfty=0
      // data->s1[0][i] = data->uInfty * data->x[0][i];
      data->s1[0][i] = data->phiLeft;

      // Rechts -> uAus = Aein / Aaus * uInfty; vAus=0
      // data->s1[data->nX - 1][i] = uOut * data->x[data->nX - 1][i];
      data->s1[data->nX - 1][i] = data->phiRight;
    }

    // Unten + Oben
    // for (int i = 1; i < data->nX - 1; ++i) {
    //   // Unten
    //   data->s1[i][0] = 1;
    //   // Oben
    //   data->s1[i][data->nY - 1] = 1;
    // }
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  /* Von alter Version
  if (data->potentialFunc == PARALLELFLOW) {
    // Links + Rechts
    for (int i = 0; i < data->nY; ++i) {
      // Links
      data->s1[0][i] =
          data->uInfty * data->x[0][i] + data->vInfty * data->y[0][i];
      // Rechts
      data->s1[data->nX - 1][i] = data->uInfty * data->x[data->nX - 1][i] +
                                  data->vInfty * data->y[data->nX - 1][i];
    }

    // Unten + Oben
    for (int i = 0; i < data->nX; ++i) {
      // Unten
      data->s1[i][0] =
          data->uInfty * data->x[i][0] + data->vInfty * data->y[i][0];
      // Oben
      data->s1[i][data->nY - 1] = data->uInfty * data->x[i][data->nY - 1] +
                                  data->vInfty * data->y[i][data->nY - 1];
    }
  } else if (data->potentialFunc == PARALLELANDSTAGNANT) {
    // Links + Rechts
    for (int i = 0; i < data->nY; ++i) {
      // Links
      data->s1[0][i] =
          data->uInfty * data->x[0][i] + data->vInfty * data->y[0][i] +
          data->a * (pow(data->x[0][i], 2) - pow(data->y[0][i], 2));
      // Rechts
      data->s1[data->nX - 1][i] = data->uInfty * data->x[data->nX - 1][i] +
                                  data->vInfty * data->y[data->nX - 1][i] +
                                  data->a * (pow(data->x[data->nX - 1][i], 2) -
                                             pow(data->y[data->nX - 1][i], 2));
    }

    // Unten + Oben
    for (int i = 0; i < data->nX; ++i) {
      // Unten
      data->s1[i][0] =
          data->uInfty * data->x[i][0] + data->vInfty * data->y[i][0] +
          data->a * (pow(data->x[i][0], 2) - pow(data->y[i][0], 2));
      // Oben
      data->s1[i][data->nY - 1] = data->uInfty * data->x[i][data->nY - 1] +
                                  data->vInfty * data->y[i][data->nY - 1] +
                                  data->a * (pow(data->x[i][data->nY - 1], 2) -
                                             pow(data->y[i][data->nY - 1], 2));
    }
  }
  */
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  std::cout << "done.\n\n";

  //-initial velocity----------------------------------------------------------

  std::cout << "\tInitialize velocity... ";
  if (data->potentialFunc == PARALLELFLOW) {
    for (int i = 0; i < data->nX; i++) {
      for (int j = 0; j < data->nY; j++) {
        data->u[i][j] = data->uInfty;
        data->v[i][j] = data->vInfty;
      }
    }
  } else if (data->potentialFunc == PARALLELANDSTAGNANT) {
    for (int i = 0; i < data->nX; i++) {
      for (int j = 0; j < data->nY; j++) {
        data->u[i][j] = data->uInfty + 2 * data->a * data->x[i][j];
        data->v[i][j] = data->vInfty - 2 * data->a * data->y[i][j];
      }
    }
  } else if (data->potentialFunc == EXAMPLE) {
    // Links + Rechts
    // double Ain = ContourMax(data->x[0][0]) - ContourMin(data->x[0][0]);
    // double Aout = ContourMax(data->x[data->nX - 1][data->nY - 1]) -
    //               ContourMin(data->x[data->nX - 1][data->nY - 1]);
    // double uOut = (Ain / Aout) * data->uInfty;

    for (int i = 0; i < data->nY; ++i) {
      // data->u[0][i] = data->uInfty;
      data->u[0][i] = 0;
      data->v[0][i] = 0;

      // data->u[data->nX - 1][i] = uOut;
      data->u[data->nX - 1][i] = 0;
      data->v[data->nX - 1][i] = 0;
    }
  }

  std::cout << "done.\n\n";

  std::cout << "\tInitialize pressure coefficient... ";
  for (int i = 0; i < data->nX; i++) {
    for (int j = 0; j < data->nY; j++) {
      data->cp[i][j] = 0;
    }
  }
  std::cout << "done.\n\n";

  // --------------------------------------------------------------------------

  return true;
}
