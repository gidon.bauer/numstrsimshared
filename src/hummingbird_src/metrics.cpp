#include "metrics.h"

#include <math.h>

#include <iostream>

#include "data.h"

//---- contour functions ------------------------------
// Upper contour
double ContourMax(double x) {
  // double y = 8;
  double y = tanh(x - 6) + 4;

  return y;
}

// Lower contour
double ContourMin(double x) {
  // double y = 0;
  double y = -tanh(x - 6) + 1;

  return y;
}

//---- coordinate transformation ----------------------
double Xi(sData* data, double x, double y) {
  double xi = (x - data->xMin) / (data->xMax - data->xMin);
  // Multiplizieren mit einem Faktor, so dass d(xi) = 1
  xi *= (data->nX - 1);
  return xi;
}

double Et(sData* data, double x, double y) {
  double eta = (y - ContourMin(x)) / (ContourMax(x) - ContourMin(x));
  // Multiplizieren mit einem Faktor, so dass d(eta) = 1
  eta *= (data->nY - 1);

  // std::cout << "x=" << x << " y=" << y << " cmax=" << ContourMax(x)
  //           << " cmin=" << ContourMin(x) << "\n";
  // std::cout << "eta = " << eta << "\n";

  return eta;
}

double X(sData* data, double xi, double et) {
  // FIXME (Subtask 2)
  return 42;
}

double Y(sData* data, double xi, double et) {
  // FIXME (Subtask 2)
  return 42;
}
