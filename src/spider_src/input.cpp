/***************************************************************************
 *   Copyright (C) 2006-2014 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "input.h"

#include <math.h>
#include <stdio.h>
#include <string.h>

#include <fstream>
#include <iostream>
#include <string>

#include "data.h"

//------------------------------------------------------
bool input(const char* cfgFilePath, sData* data) {
  char line[256] = " ", token[16] = " ";
  std::cout << "\nInput:\n-------\n";

  //////////////////////
  // READ CFG FILE    //
  //////////////////////

  // open input file
  std::ifstream cfgFile(cfgFilePath);
  if (!cfgFile) {
    return false;
  }

  // read input file line by line
  while (!cfgFile.eof()) {
    cfgFile.getline(line, 255);
    if (!sscanf(line, "%15s", token)) {
      continue;
    };

    if (!strcmp(token, "#")) {
      // skip comment lines
    } else if (!strcmp(token, "nCellsX")) {
      sscanf(line, "%15s %d", token, &data->nCellsX);
    } else if (!strcmp(token, "nCellsY")) {
      sscanf(line, "%15s %d", token, &data->nCellsY);
    } else if (!strcmp(token, "xMin")) {
      sscanf(line, "%15s %lf", token, &data->xMin);
    } else if (!strcmp(token, "xMax")) {
      sscanf(line, "%15s %lf", token, &data->xMax);
    } else if (!strcmp(token, "yMin")) {
      sscanf(line, "%15s %lf", token, &data->yMin);
    } else if (!strcmp(token, "yMax")) {
      sscanf(line, "%15s %lf", token, &data->yMax);
    } else if (!strcmp(token, "left")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token, &data->boundary_cond[LEFT],
             &data->parameter[LEFT], &data->pres[LEFT], &data->u_sides[LEFT],
             &data->v_sides[LEFT]);
    } else if (!strcmp(token, "right")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token,
             &data->boundary_cond[RIGHT], &data->parameter[RIGHT],
             &data->pres[RIGHT], &data->u_sides[RIGHT], &data->v_sides[RIGHT]);
    } else if (!strcmp(token, "up")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token, &data->boundary_cond[UP],
             &data->parameter[UP], &data->pres[UP], &data->u_sides[UP],
             &data->v_sides[UP]);
    } else if (!strcmp(token, "down")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token, &data->boundary_cond[DOWN],
             &data->parameter[DOWN], &data->pres[DOWN], &data->u_sides[DOWN],
             &data->v_sides[DOWN]);
    } else if (!strcmp(token, "upleft")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token,
             &data->boundary_cond[UPLEFT], &data->parameter[UPLEFT],
             &data->pres[UPLEFT], &data->u_sides[UPLEFT],
             &data->v_sides[UPLEFT]);
    } else if (!strcmp(token, "downleft")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token,
             &data->boundary_cond[DOWNLEFT], &data->parameter[DOWNLEFT],
             &data->pres[DOWNLEFT], &data->u_sides[DOWNLEFT],
             &data->v_sides[DOWNLEFT]);
    } else if (!strcmp(token, "upright")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token,
             &data->boundary_cond[UPRIGHT], &data->parameter[UPRIGHT],
             &data->pres[UPRIGHT], &data->u_sides[UPRIGHT],
             &data->v_sides[UPRIGHT]);
    } else if (!strcmp(token, "downright")) {
      sscanf(line, "%15s %d %lf %lf %lf %lf", token,
             &data->boundary_cond[DOWNRIGHT], &data->parameter[DOWNRIGHT],
             &data->pres[DOWNRIGHT], &data->u_sides[DOWNRIGHT],
             &data->v_sides[DOWNRIGHT]);
    } else if (!strcmp(token, "sc")) {
      sscanf(line, "%15s %lf", token, &data->sc_begin);
    } else if (!strcmp(token, "p")) {
      sscanf(line, "%15s %lf", token, &data->p_begin);
    } else if (!strcmp(token, "u")) {
      sscanf(line, "%15s %lf", token, &data->u_begin);
    } else if (!strcmp(token, "v")) {
      sscanf(line, "%15s %lf", token, &data->v_begin);
    }

    // FIXME
    // Add new keywords here!
    else {
      std::cout << "unknown token: " << token << std::endl;
    }
  }
  cfgFile.close();

  return true;
}
