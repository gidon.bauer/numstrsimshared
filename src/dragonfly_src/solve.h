/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef CALC_H
#define CALC_H

#include "data.h"

bool solve(sData* data);
void calcFlux(sData* data);
void precomputeMomentumCoeffs(sData* data);
void calcMomentum(sData* data, double dt);
double calcPCorr(sData* data, double dt);

double upwind_flux(sData* data, double dx, double dy, double u, double v,
                   double sc_M, double sc_P);

double central_flux(sData* data, double dx, double dy, double u, double v,
                    double sc_M, double sc_P);

double analytical_flux(sData* data, double dx, double dy, double x, double y,
                       double x_M, double y_M, double u, double v, double sc_M,
                       double sc_P);

void precomputeScalarCeoffs(sData* data);

double calc_A(sData* data, double Pe);

#endif
