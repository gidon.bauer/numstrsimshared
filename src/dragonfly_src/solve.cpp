/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "solve.h"

#include <math.h>
#include <stdio.h>

#include <iostream>

#include "data.h"
#include "output.h"

//------------------------------------------------------
bool solve(sData* data) {
  std::cout << "\nCalculation:\n------------\n";
  std::cout << "\tMode: ";
  switch (data->mode) {
    case CALCFLUX:
      std::cout << "calcFlux.\n";
      break;

    case ALGEBRAIC:
      std::cout << "Algebraic.\n";
      break;

    default:
      std::cerr << "Unknown mode.\n";
      return false;
  }

  std::cout << "\tMethod for numerical flux: ";
  switch (data->method) {
    case UPWIND:
      std::cout << "Upwind.\n";
      break;

    case CENTRAL:
      std::cout << "Central.\n";
      break;

    case ANALYTICAL:
      std::cout << "Analytical.\n";
      break;

    case HYBRID:
      std::cout << "Hybrid.\n";
      break;

    case POWER:
      std::cout << "Power.\n";
      break;

    case EXPONENTIAL:
      std::cout << "Exponential.\n";
      break;

    default:
      std::cerr << "Unknown method.\n";
      return false;
  }

  double curTime = 0;
  sCell* curCell = NULL;
  sFace* curFace = NULL;

  // dump inital fields
  if (!output(data, curTime)) {
    std::cout << "ERROR while data output...exiting";
    getchar();
    return 1;
  }

  // Time Loop
  while (curTime <= (data->maxTime - data->dt)) {
    // advance time
    curTime += data->dt;
    std::cout << "\r\tSolving ... current Time: " << curTime << " s\n";

    // FIXME
    // M2: solve scalar transport equation

    // -Using calcFlux()--------------------------------------------------
    if (data->mode == CALCFLUX) {
      calcFlux(data);
      for (int cellId = 0; cellId < data->nCells; ++cellId) {
        curCell = &data->cells[cellId];
        // Summe der ein- und ausströmenden Flüsse
        // Annahme: kartesisches Gitter
        if ((curCell->bType_sc == INNERCELL) ||
            (curCell->bType_sc == NEUMANN)) {
          double sum_flux =
              (curCell->faces[YP]->numFlux - curCell->faces[YM]->numFlux) *
                  curCell->faces[YM]->dx +
              (curCell->faces[XP]->numFlux - curCell->faces[XM]->numFlux) *
                  curCell->faces[XM]->dy;

          // Explizites Euler-Verfahren
          curCell->sc -= (data->dt / (data->rho * curCell->vol)) * sum_flux;
        }
      }
    }

    // -Algebraischer Ansatz--------------------------------------------
    else if (data->mode == ALGEBRAIC) {
      // -sc------------------------------------------------------------
      precomputeScalarCeoffs(data);

      // double sc_new[data->nCells];
      // for (int i = 0; i < data->nCells; ++i) {
      //   sc_new[i] = data->cells[i].sc;
      // }

      double b;
      for (int cellId = 0; cellId < data->nCells; ++cellId) {
        curCell = &data->cells[cellId];

        if ((curCell->bType_sc == INNERCELL) ||
            (curCell->bType_sc == NEUMANN)) {
          b = curCell->sc * curCell->a[C];

          // sc_new[cellId] = b / curCell->a[T];
          curCell->sc = b / curCell->a[T];
          for (int i = 0; i < 4; ++i) {
            if (curCell->neighCells[i]) {
              // sc_new[cellId] +=
              //     (curCell->a[i] * curCell->neighCells[i]->sc) /
              //     curCell->a[T];
              curCell->sc +=
                  (curCell->a[i] * curCell->neighCells[i]->sc) / curCell->a[T];
            } else {
              // Falsch -> wahrscheinlich muss der Wert für sc in der Zelle
              // außerhalb des Gitters berechnet werden

              // Nur für neumannFlux = 0 => SOLID
              curCell->sc += (curCell->a[i] * curCell->sc) / curCell->a[T];
            }
          }
        }
      }
      // for (int i = 0; i < data->nCells; ++i) {
      //   data->cells[i].sc = sc_new[i];
      // }

      // -u/v------------------------------------------------------------
      // FIXME
      // M3: solve momentum equation for fixed pressure field
      // precomputeMomentumCoeffs(data);
      // // double b;
      // double press_grad;
      // sFace* neFaces[4];
      // for (int faceId = 0; faceId < data->nFaces; ++faceId) {
      //   curFace = &data->faces[faceId];

      //   // eventuell auch für NEUMANN Faces berechnen
      //   if ((curFace->bType_u == INNERCELL) ||
      //       (curFace->bType_u == DIRICHLET)) {
      //     // Gucken in welche Richtung der Fluss geht
      //     // Fluss in y-Richtung
      //     if (curFace->dx > EPS) {
      //       neFaces[YP] = curFace->neighCells[P]->faces[YP];
      //       neFaces[XP] = curFace->neighCells[M]->neighCells[XM]->faces[YP];
      //       neFaces[YM] = curFace->neighCells[M]->faces[YM];
      //       neFaces[XM] = curFace->neighCells[M]->neighCells[XP]->faces[YP];

      //       b = curFace->v * curFace->a[C];
      //       press_grad =
      //           (curFace->neighCells[M]->p - curFace->neighCells[P]->p) *
      //           curFace->dx;

      //       curFace->v = (b + press_grad) / curFace->a[T];
      //       for (int i = 0; i < 4; ++i) {
      //         if (neFaces[i]) {
      //           curFace->v += (curFace->a[i] * neFaces[i]->v) /
      //           curFace->a[T];
      //         } else {
      //           std::cerr << "ERROR.\n";
      //           // Falsch -> wahrscheinlich muss der Wert für sc in der Zelle
      //           // außerhalb des Gitters berechnet werden
      //           // curFace->sc += curFace->faces[i]->neumannFlux /
      //           // curFace->a[T];
      //         }
      //       }
      //     }
      //     // Fluss in x_Richtung
      //     else {
      //       neFaces[YP] = curFace->neighCells[M]->neighCells[YP]->faces[XP];
      //       neFaces[XP] = curFace->neighCells[P]->faces[XP];
      //       neFaces[YM] = curFace->neighCells[M]->neighCells[YM]->faces[XP];
      //       neFaces[XM] = curFace->neighCells[M]->faces[XM];

      //       b = curFace->u * curFace->a[C];
      //       press_grad =
      //           (curFace->neighCells[M]->p - curFace->neighCells[P]->p) *
      //           curFace->dy;

      //       curFace->u = (b + press_grad) / curFace->a[T];
      //       for (int i = 0; i < 4; ++i) {
      //         if (neFaces[i]) {
      //           curFace->u += (curFace->a[i] * neFaces[i]->u) /
      //           curFace->a[T];
      //         } else {
      //           std::cerr << "ERROR.\n";
      //           // Falsch -> wahrscheinlich muss der Wert für sc in der Zelle
      //           // außerhalb des Gitters berechnet werden
      //           // curFace->sc += curFace->faces[i]->neumannFlux /
      //           // curFace->a[T];
      //         }
      //       }
      //     }
      //   }
      // }
      // -p------------------------------------------------------------

      // FIXME
      // M4: SIMPLE solver loop to solve full incompressible NS-equations
    }

    // dump fields after time step
    if (!output(data, curTime)) {
      std::cout << "ERROR while data output...exiting";
      getchar();
      return 1;
    }

  }  // end Time loop

  std::cout << "\n";
  return true;
}

//-calcFlux---------------------------------------------------------------
void calcFlux(sData* data) {
  /*
    1) Celltype: inner, dirichlet, neumann oder solid
    2) Methode zur Berechnung: Upwind, Central oder Analyitical
  */

  static sFace* curFace = 0;

  for (int fId = 0; fId < data->nFaces; fId++) {
    // FIXME M2
    // compute numerical flux over each face
    curFace = &data->faces[fId];

    // -Celltype: Inner Cell or Dirichlet------------------------------------
    if (curFace->bType_u == INNERCELL || curFace->bType_u == DIRICHLET) {
      // --Upwind------------------------------------------------------------
      if (data->method == UPWIND) {
        curFace->numFlux =
            upwind_flux(data, curFace->dx, curFace->dy, curFace->u, curFace->v,
                        curFace->neighCells[M]->sc, curFace->neighCells[P]->sc);
      }
      // --Zentrale Differenzen----------------------------------------------
      else if (data->method == CENTRAL) {
        curFace->numFlux = central_flux(
            data, curFace->dx, curFace->dy, curFace->u, curFace->v,
            curFace->neighCells[M]->sc, curFace->neighCells[P]->sc);
      }
      // --Analytische Lösung------------------------------------------------
      else if (data->method == ANALYTICAL) {
        curFace->numFlux = analytical_flux(
            data, curFace->dx, curFace->dy, curFace->x, curFace->y,
            curFace->neighCells[M]->x, curFace->neighCells[M]->y, curFace->u,
            curFace->v, curFace->neighCells[M]->sc, curFace->neighCells[P]->sc);
      }
    }
    // -Celltype: Neumann----------------------------------------------------
    else if (curFace->bType_u == NEUMANN) {
      // std::cout << curFace->id << " Neumann\n";
      curFace->numFlux = curFace->neumannFlux;
    }
    // -Celltype: Solid------------------------------------------------------
    else if (curFace->bType_u == SOLID) {
      curFace->numFlux = 0;
    }
    // -Celltype: Invalid----------------------------------------------------
    else {
      throw std::runtime_error("Invalid celltype.");
    }
  }
}

//------------------------------------------------------------------------

void precomputeScalarCeoffs(sData* data) {
  static sCell* curCell = 0;

  for (int cId = 0; cId < data->nCells; ++cId) {
    curCell = &data->cells[cId];
    const int nNeigh = 4;
    double fNeigh[nNeigh];

    double h, vel, D, Pe, A;

    curCell->a[T] = 0;
    for (int i = 0; i < nNeigh; ++i) {
      h = (curCell->faces[i]->dx > 0) ? curCell->faces[i]->dx
                                      : curCell->faces[i]->dy;

      vel = (curCell->faces[i]->dx > 0) ? curCell->faces[i]->v
                                        : curCell->faces[i]->u;

      fNeigh[i] = data->rho * vel;

      D = data->alpha / h;

      Pe = (data->rho * vel * h) / data->alpha;
      // Pe = (vel * h) / data->alpha;

      if (ABS(data->alpha) <= EPS) {
        A = 0;
      } else {
        A = calc_A(data, Pe);
      }

      curCell->a[i] = D * h * A;
      curCell->a[i] +=
          (i == XM || i == YM) ? MAX(fNeigh[i] * h, 0) : MAX(-fNeigh[i] * h, 0);

      curCell->a[T] += curCell->a[i];
    }
    if ((fNeigh[XP] - fNeigh[XM]) >= EPS || (fNeigh[YP] - fNeigh[YM]) >= EPS) {
      // std::cerr << "Something went wrong in Cell " << cId << ":"
      //           << "\nf_e - f_w = " << fNeigh[XP] - fNeigh[XM]
      //           << "\nf_n - f_s = " << fNeigh[YP] - fNeigh[YM] << "\n";
    }
    // Berechnen von ap und ap_tilde
    curCell->a[C] = (data->rho * curCell->vol) / data->dt;
    curCell->a[T] += curCell->a[C];
    // std::cout << cId << ": a = " << curCell->a << "\n";
  }
}

// -----------------------------------------------------------------------

void precomputeMomentumCoeffs(sData* data) {
  static sFace* curFace = 0;

  for (int fId = 0; fId < data->nFaces; fId++) {
    curFace = &data->faces[fId];

    // Überspringt Faces mit Typ NEUMANN oder SOlID
    if (curFace->bType_u == NEUMANN || curFace->bType_u == SOLID) continue;

    const int nNeigh = 4;
    double fNeigh[nNeigh];

    /*
      Kante mit Fluss in x-Richtung:
        N: curFace->neighCells[M]->neighCells[YP]->faces[XP]
        E: curFace->neighCells[P]->faces[XP]
        S: curFace->neighCells[M]->neighCells[YM]->faces[XP]
        W: curFace->neighCells[M]->faces[XM]

      Kante mit Fluss in x-Richtung:
        N: curFace->neighCells[P]->faces[YP]
        E: curFace->neighCells[M]->neighCells[XM]->faces[YP]
        S: curFace->neighCells[M]->faces[YM]
        W: curFace->neighCells[M]->neighCells[XP]->faces[YP]

      Bei uns entspricht:
        YP = N
        XP = E
        YM = S
        XM = W
    */
    sFace* neFaces[nNeigh];

    // Gucken in welche Richtung der Fluss geht
    // Fluss in y-Richtung
    if (curFace->dx > EPS) {
      neFaces[YP] = curFace->neighCells[P]->faces[YP];
      neFaces[XP] = curFace->neighCells[M]->neighCells[XM]->faces[YP];
      neFaces[YM] = curFace->neighCells[M]->faces[YM];
      neFaces[XM] = curFace->neighCells[M]->neighCells[XP]->faces[YP];
    }
    // Fluss in x-Richtung
    else {
      neFaces[YP] = curFace->neighCells[M]->neighCells[YP]->faces[XP];
      neFaces[XP] = curFace->neighCells[P]->faces[XP];
      neFaces[YM] = curFace->neighCells[M]->neighCells[YM]->faces[XP];
      neFaces[XM] = curFace->neighCells[M]->faces[XM];
    }

    // Notwendige Variablen
    double h, vel, D, Pe, A;
    curFace->a[T] = 0;
    for (int i = 0; i < nNeigh; ++i) {
      h = MAX(neFaces[i]->dx, neFaces[i]->dy);

      vel = (neFaces[i]->dx > 0) ? neFaces[i]->v : neFaces[i]->u;

      fNeigh[i] = data->rho * vel;

      D = data->alpha / h;

      Pe = (data->rho * vel * h) / data->alpha;
      // Pe = (vel * h) / data->alpha;

      if (ABS(data->alpha) <= EPS) {
        A = 0;
      } else {
        A = calc_A(data, Pe);
      }

      curFace->a[i] = D * h * A;
      curFace->a[i] +=
          (i == XM || i == YM) ? MAX(fNeigh[i] * h, 0) : MAX(-fNeigh[i] * h, 0);

      curFace->a[T] += curFace->a[i];
    }

    if ((fNeigh[XP] - fNeigh[XM]) >= EPS || (fNeigh[YP] - fNeigh[YM]) >= EPS) {
      // std::cerr << "Something went wrong on Face " << fId << ":"
      //           << "\nf_e - f_w = " << fNeigh[XP] - fNeigh[XM]
      //           << "\nf_n - f_s = " << fNeigh[YP] - fNeigh[YM] << "\n";
    }
    // Berechnen von ap und ap_tilde

    double vol = curFace->neighCells[M]->vol;
    // gleich dem Volumen der nachfolgenden Zelle ->
    // muss für nicht gleiche Zellen geändert werden

    curFace->a[C] = (data->rho * vol) / data->dt;
    curFace->a[T] += curFace->a[C];
  }
}

void calcMomentum(sData* data, double dt) {
  static sFace* curFace = 0;
  double curMaxResidual = MAXDOUBLE;
  double curResidual = MAXDOUBLE;
  int curIter = 0;

  // while(curIter<data->maxIter && ABS( curMaxResidual ) > data->residual ) {
  while (ABS(curMaxResidual) > data->residual) {
    curIter++;
    curMaxResidual = 0;

    for (int fId = 0; fId < data->nFaces; fId++) {
      curFace = &data->faces[fId];

      // FIXME M3
      // calculate face velocities from momentum equation

      if (ABS(curResidual) > ABS(curMaxResidual))
        curMaxResidual = ABS(curResidual);
    }
  }
}

double calcPCorr(sData* data, double dt) {
  static sCell* curCell = 0;
  double curMaxResidual = MAXDOUBLE;
  double curResidual = MAXDOUBLE;
  double curMassErr = 0;
  int curIter = 0;

  while (ABS(curMaxResidual) > data->residual) {
    curIter++;
    curMaxResidual = 0;

    for (int cId = 0; cId < data->nCells; cId++) {
      curCell = &data->cells[cId];

      // FIXME M4
      // calculate pressure correction

      if (ABS(curResidual) > ABS(curMaxResidual))
        curMaxResidual = ABS(curResidual);
    }
  }
  return curMassErr;
}

// -Methoden zur Berechnung des numerischen Flux--------------------------
double upwind_flux(sData* data, double dx, double dy, double u, double v,
                   double sc_M, double sc_P) {
  double h = (dx > 0) ? dx : dy;
  double vel = (dx > 0) ? v : u;

  if (vel > 0) {
    return data->rho * vel * sc_M - data->alpha * (sc_P - sc_M) / h;
  } else {
    return data->rho * vel * sc_P - data->alpha * (sc_P - sc_M) / h;
  }
};

double central_flux(sData* data, double dx, double dy, double u, double v,
                    double sc_M, double sc_P) {
  double h = (dx > 0) ? dx : dy;
  double vel = (dx > 0) ? v : u;

  return data->rho * vel * (1.0 / 2.0) * (sc_M + sc_P) -
         data->alpha * (sc_P - sc_M) / h;
};

double analytical_flux(sData* data, double dx, double dy, double x, double y,
                       double x_M, double y_M, double u, double v, double sc_M,
                       double sc_P) {
  /*
    phi     = (phiL - phi0) * (exp(Pe*y/l) - 1)/(exp(Pe) - 1) + phi0
    dphi/dy = (phiL - phi0) * (Pe/l * exp(Pe*y/l))/(exp(Pe) - 1)
  */

  double l = (dx > 0) ? dx : dy;
  double vel = (dx > 0) ? v : u;

  double Pe;
  // Blöder Fix
  if (ABS(data->alpha) <= EPS) {
    Pe = MAXDOUBLE;
  } else {
    Pe = (data->rho * vel * l) / data->alpha;
  }
  double pos_star = (dx > 0) ? y - y_M : x - x_M;

  double phi_star = 0, dphi_stardy = 0;
  if (ABS(Pe) >= EPS) {
    phi_star =
        (sc_P - sc_M) * ((exp((Pe * pos_star) / l) - 1) / (exp(Pe) - 1)) + sc_M;
    dphi_stardy =
        (sc_P - sc_M) * (((Pe / l) * exp((Pe * pos_star) / l)) / (exp(Pe) - 1));
  }
  // Betrachte Grenzwerte für Pe->0
  else {
    phi_star = (sc_P - sc_M) * (pos_star / l) + sc_M;
    dphi_stardy = (sc_P - sc_M) / l;
  }

  return data->rho * vel * phi_star - data->alpha * dphi_stardy;
}

double calc_A(sData* data, double Pe) {
  double A;
  switch (data->method) {
    case CENTRAL:
      A = 1 - 0.5 * ABS(Pe);
      break;

    case UPWIND:
      A = 1;
      break;

    case HYBRID:
      A = MAX(0, 1 - 0.5 * ABS(Pe));
      break;

    case POWER:
      A = MAX(0, pow(1 - 0.1 * ABS(Pe), 5));
      break;

    case EXPONENTIAL:
      if (ABS(Pe) <= EPS) {
        A = 1;
      } else {
        A = ABS(Pe) / (exp(ABS(Pe)) - 1);
      }
      break;

    default:
      throw std::runtime_error("Invalid method for algebraic mode.");
  }

  return A;
}